import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Arrays;

public class Pierwsze {
    private static String inputString="   78, 6 5, 4, 7 ,1";

    ArrayList<Integer> findPrimeNums(String inputNums) throws EmptyDataException {

        if (inputNums==null || inputNums.equals("")) {
            throw new EmptyDataException();                                 //sprawdzamy czy podany string nie jest pusty
        }
        inputNums = inputNums.replaceAll(" ", "");      //usuwamy znaki biale ze stringa
        String[] splitArray = inputNums.split(",");                 //wrzucamy liczby to tablicy

        ArrayList <Integer> numbers = new ArrayList<>();                //tworzymy liste

        for (String element: splitArray) {
            numbers.add(Integer.parseInt(element));                     //do listy dodajemy elementy z tablicy i parsujemy na int
        }

        ArrayList <Integer> primeNum = new ArrayList<>();               //lista ktora bedzie przechowywac liczby pierwsze

        for (Integer n:numbers) {                               //algorytm na wyznaczanie liczb pierwszych
            if (n >= 1) {
                for (int i=2; i<n; i++) {
                    if (n%i != 0) {
                        primeNum.add(n);
                    }
                    break;
                }
            }
        }
        return primeNum;
    }

    public static void main(String[] args) {
        Pierwsze p = new Pierwsze();
        try
        {
            System.out.println(p.findPrimeNums(inputString));
        } catch (EmptyDataException ede) {
            ede.printStackTrace();
            }

    }
}
